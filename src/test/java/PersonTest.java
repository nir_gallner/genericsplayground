import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

public class PersonTest {

    Person donDraper;
    Person peggyOlson;
    Person bertCooper;

    @Before
    public void setUp() {
        donDraper = new Person("Don Draper", 89);
        peggyOlson = new Person("Peggy Olson", 65);
        bertCooper = new Person("Bert Cooper", 100);
    }

    @Test
    public void listExampleIterators() {


        List<Person> madMen = new ArrayList<Person>();
        madMen.add(donDraper);
        madMen.add(peggyOlson);
        madMen.add(bertCooper);

        final Iterator<Person> iterator = madMen.iterator();
        while (iterator.hasNext()) {
            final Person person = iterator.next();
            System.out.println(person);
        }
    }

    @Test
    public void listExampleforEach() {

        List<Person> madMen = new ArrayList<>();
        madMen.add(donDraper);
        madMen.add(peggyOlson);
        madMen.add(bertCooper);

        for (Person p : madMen) {
            System.out.println(p);
        }
    }

    @Test
    public void setExample() {

        // A set does not allow duplicates
        Set<Person> madMen = new HashSet<>();
        madMen.add(donDraper);
        madMen.add(peggyOlson);
        madMen.add(bertCooper);
        madMen.add(donDraper);

        for (Person p : madMen) {
            System.out.println(p);
        }
    }

    @Test
    public void mapExample() {

        // A set does not allow duplicates
        Map<String, Person> madMen = new HashMap<>();
        madMen.put(donDraper.getName(), donDraper);
        madMen.put(peggyOlson.getName(), peggyOlson);
        madMen.put(bertCooper.getName(), bertCooper);

        System.out.println(madMen);
    }

    @Test
    public void mapIterateExample() {

        // A set does not allow duplicates
        Map<String, Person> madMen = new HashMap<>();
        madMen.put(donDraper.getName(), donDraper);
        madMen.put(peggyOlson.getName(), peggyOlson);
        madMen.put(bertCooper.getName(), bertCooper);

        for (String name : madMen.keySet())
            System.out.println(name);

        for (Person person : madMen.values())
            System.out.println(person);

        for (Map.Entry<String, Person> entry : madMen.entrySet()) {
            System.out.println(entry.getValue());
            System.out.println(entry.getKey());
        }
    }

    @Test
    public void collectionSort() {

        List<Person> madMen = new ArrayList<>();
        madMen.add(donDraper);
        madMen.add(peggyOlson);
        madMen.add(bertCooper);

        System.out.println("initial order: \n");
        System.out.println(madMen);

        Collections.sort(madMen,new AgeComperator());

        System.out.println("After sort: \n");
        System.out.println(madMen);
    }

    @Test
    public void collectionSortAnonymous() {

        List<Person> madMen = new ArrayList<>();
        madMen.add(donDraper);
        madMen.add(peggyOlson);
        madMen.add(bertCooper);

        System.out.println("initial order: \n");
        System.out.println(madMen);

        Collections.sort(madMen, new Comparator<Person>() {
            @Override
            public int compare(final Person left, final Person right) {
                return Integer.compare(left.getAge(), right.getAge());
            }
        });

        System.out.println("After sort: \n");
        System.out.println(madMen);
    }

    @Test
    public void collectionReverseSortAnonymous() {

        List<Person> madMen = new ArrayList<>();
        madMen.add(donDraper);
        madMen.add(peggyOlson);
        madMen.add(bertCooper);

        System.out.println("initial order: \n");
        System.out.println(madMen);

        Collections.sort(madMen, new ReverseComparator<>(new AgeComperator()));

        System.out.println("After sort: \n");
        System.out.println(madMen);
    }



}
