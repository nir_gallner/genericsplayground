import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class SortedPairTest {

    @Test
    public void shouldRetainOrderOfSortedPair() {
        SortedPair<Integer> pair = new SortedPair<>(2, 4);
        Assert.assertEquals(2, pair.getFirst().intValue());
        Assert.assertEquals(4, pair.getSecond().intValue());
    }

    @Test
    public void shouldFlipOrderOfSortedPair() {
        SortedPair<Integer> pair = new SortedPair<>(4, 2);
        Assert.assertEquals(2, pair.getFirst().intValue());
        Assert.assertEquals(4, pair.getSecond().intValue());
    }

    @Test
    public void minTest() {
        List<Integer> listOfInts = new ArrayList<>();

        listOfInts.add(3);
        listOfInts.add(1);
        listOfInts.add(32);
        listOfInts.add(-3);

        Integer min = min(listOfInts, Integer::compare);

        Assert.assertEquals(-3, min.intValue());
    }

    public static <T> T min(List<T> values, Comparator<T> comparator) {
        if (values.isEmpty())
            throw new IllegalArgumentException("List is empty");

        T lowestElement = values.get(0);
        for (int i = 1; i < values.size(); i++) {
            final T element = values.get(i);
            if (comparator.compare(element, lowestElement) < 0)
                lowestElement = element;
        }
        return lowestElement;
    }
}
