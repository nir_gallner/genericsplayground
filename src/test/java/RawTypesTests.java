import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RawTypesTests {

    @Test
    public void rawTypesList(){
        List list = new ArrayList();
        list.add("abc");
        list.add(1);
        list.add(new Object());


        Iterator it = list.iterator();
        while (it.hasNext()){
            Object o = it.next();
            System.out.println(o);
        }
    }
}
