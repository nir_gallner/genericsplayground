import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class SaveLoadTests {

    private Partner donHarper = new Partner("Don Harper", 89);
    private Partner bertCooper = new Partner("Bert Cooper", 100);
    private Employee peggyOlson = new Employee("Peggy Olson", 65);

    private File file;
    private PersonSaver saver;
    private PersonLoader loader;

    @Test
    public void saverAndLoadPerson() throws IOException, ClassNotFoundException {
        Person person  = new Person("Bob", 28);
        saver.save(person);

        assertEquals(person, loader.load());

    }
}
