import java.util.Comparator;

public class AgeComperator implements Comparator<Person>{

    @Override
    public int compare(final Person left, final Person right) {
        return Integer.compare(left.getAge(), right.getAge());
    }
}
