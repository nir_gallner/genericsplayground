package co.verisoft.ooplayground;

public final class UserPage extends BasePage {

    public UserPage(String driver) {
        super(driver);
    }


    //test 1
    public static void main(String[]  args){

        // this is in the before
        String driver1 = "chrome";

        UserPage user = new UserPage(driver1);
        DashboardPage dash = new DashboardPage(driver1);

        System.out.println(user.getDriver());
        System.out.println(dash.getDriver());
    }
}
