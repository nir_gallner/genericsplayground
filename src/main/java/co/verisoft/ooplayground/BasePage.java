package co.verisoft.ooplayground;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

 public abstract class BasePage {

    protected String driver;

    public BasePage(String driver){
        this.driver = driver;
    }

    public String getDriver(){
        return this.driver;
    }

}
